import Config

config :league_of_pids, LeagueOfPidsWeb.Endpoint,
  url: [scheme: "https", host: System.fetch_env!("LEAGUE_OF_PIDS_HOST"), port: 443],
  http: [
    port: String.to_integer(System.get_env("LEAGUE_OF_PIDS_PORT") || "4001"),
    transport_options: [socket_opts: [:inet6]]
  ],
  live_view: [
    signing_salt: System.fetch_env!("LEAGUE_OF_PIDS_SECRET_KEY_BASE")
  ],
  secret_key_base: System.fetch_env!("LEAGUE_OF_PIDS_SECRET_KEY_BASE"),
  server: !System.get_env("LEAGUE_OF_PIDS_IEX")
