# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

# Configures the endpoint
config :league_of_pids, LeagueOfPidsWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "bTHfz93zdpmXRGlBeBljksnG1i4zRNItaDfVH4vZ/DPXiVzU8XDpC/7ime0kBfWi",
  render_errors: [view: LeagueOfPidsWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: LeagueOfPids.PubSub,
  live_view: [signing_salt: "ssuVgZpC"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
