# League of PIDs

League of PIDs is a massively multiplayer online game (MMO) where heroes kill or get killed by enemies.

## Setup

  * `make start` to build and start the container.
  * `make setup` to run the setup script.

## Common tasks

You can find many common task definitions in the `Makefile`. You can either use them, or start bash with `make shell` and use `mix` or other commands.

### Start server

```bash
make server
```

### Run tests

```bash
make test
```

### Mix dependencies
Get  dependencies
```bash
make deps
```

Update all dependencies
```bash
make deps_update
```

Clean unused dependencies
```bash
make deps_clean
```

### IEx console

```bash
make console
```
To exit you need to press `Ctrl+C` twice.

### Format Elixir code

```bash
make format
```
