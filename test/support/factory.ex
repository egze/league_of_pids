defmodule LeagueOfPids.Factory do
  defmacro __using__(_options) do
    quote do
      alias LeagueOfPids.Core.{Arena, Game, Hero}

      import LeagueOfPids.Factory, only: :functions
    end
  end

  alias LeagueOfPids.Core.{Arena, Game, Hero}

  def build_hero(overrides \\ []) do
    overrides
    |> hero_fields()
    |> Hero.new()
  end

  def hero_fields(overrides \\ []) do
    Keyword.merge(
      [
        name: "jose_valim",
        position: {0, 0}
      ],
      overrides
    )
  end

  def build_arena(overrides \\ []) do
    overrides
    |> arena_fields()
    |> Arena.new()
  end

  def arena_fields(overrides \\ []) do
    Keyword.merge(
      [
        size: {5, 5}
      ],
      overrides
    )
  end

  def build_game(overrides \\ []) do
    overrides
    |> game_fields()
    |> Game.new()
  end

  def game_fields(overrides \\ []) do
    Keyword.merge(
      [
        arena: build_arena()
      ],
      overrides
    )
  end
end
