defmodule LeagueOfPids.Boundary.GameManagerTest do
  use ExUnit.Case

  alias LeagueOfPids.Boundary.{GameManager, HeroManager}
  alias LeagueOfPids.Core.Game

  import GameManager

  test "returns game" do
    assert %Game{} = get_game()
  end

  test "returns heroes" do
    start_supervised!({HeroManager, "hulk"})
    start_supervised!({HeroManager, "iron_man"})

    heroes = get_heroes()
    assert Enum.count(heroes) == 2
  end
end
