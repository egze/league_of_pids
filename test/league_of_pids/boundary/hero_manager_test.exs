defmodule LeagueOfPids.Boundary.HeroManagerTest do
  use ExUnit.Case

  alias LeagueOfPids.Boundary.{GameManager, HeroManager}
  alias LeagueOfPids.Core.Hero

  import HeroManager

  setup _context do
    {:ok, %{game: GameManager.get_game()}}
  end

  test "returns hero" do
    start_supervised!({HeroManager, "hulk"})

    assert %Hero{name: "hulk"} = get_hero("hulk")
  end

  test "moves hero", %{game: game} do
    start_supervised!({HeroManager, "hulk"})

    hero = move_hero("hulk", {game, "up"})
    assert hero.position == {1, 2}
  end

  test "kills hero" do
    start_supervised!({HeroManager, "hulk"})
    start_supervised!({HeroManager, "iron_man"})

    Phoenix.PubSub.subscribe(LeagueOfPids.PubSub, "game:hero_killed")
    :ok = shoot("hulk")

    assert_receive({:hero_killed, %Hero{name: "iron_man"}}, 1_000)
    assert %Hero{name: "iron_man", alive: false} = get_hero("iron_man")
    assert %Hero{name: "hulk", alive: true} = get_hero("hulk")
  end

  test "does not kill hero when far away", %{game: game} do
    start_supervised!({HeroManager, "hulk"})
    start_supervised!({HeroManager, "iron_man"})

    Phoenix.PubSub.subscribe(LeagueOfPids.PubSub, "game:hero_killed")

    move_hero("hulk", {game, "up"})
    move_hero("hulk", {game, "up"})
    :ok = shoot("hulk")

    refute_receive({:hero_killed, %Hero{name: "iron_man"}}, 1_000)
    assert %Hero{name: "iron_man", alive: true} = get_hero("iron_man")
    assert %Hero{name: "hulk", alive: true} = get_hero("hulk")
  end
end
