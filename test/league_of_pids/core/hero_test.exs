defmodule LeagueOfPids.Core.HeroTest do
  use ExUnit.Case, async: true

  use LeagueOfPids.Factory

  import Hero

  test "builds hero" do
    assert %Hero{} = build_hero()
  end

  test "hero can move" do
    hero = build_hero()

    position_left = position_left(hero)
    hero = move_to_position(hero, position_left)
    assert hero.position == {0, 1}

    position_up = position_up(hero)
    hero = move_to_position(hero, position_up)
    assert hero.position == {0, 2}
  end

  test "survives missed shot" do
    build_hero(position: {1, 1})
    |> shot_fired({5, 5})
    |> assert_hero_alive()
  end

  test "killed be perfect shot" do
    build_hero(position: {1, 1})
    |> shot_fired({1, 1})
    |> assert_hero_dead()
  end

  test "killed be nearby blast" do
    build_hero(position: {2, 1})
    |> shot_fired({1, 1})
    |> assert_hero_dead()
  end

  defp assert_hero_alive(hero) do
    assert hero.alive
    hero
  end

  defp assert_hero_dead(hero) do
    refute hero.alive
    hero
  end
end
