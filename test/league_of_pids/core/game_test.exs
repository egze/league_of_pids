defmodule LeagueOfPids.Core.GameTest do
  use ExUnit.Case, async: true

  use LeagueOfPids.Factory

  import Game

  setup [:game, :hero]

  test "builds game", %{game: game} do
    assert %Game{} = game
  end

  describe "movement" do
    test "performs basic movement", %{game: game, hero: hero} do
      game
      |> move_hero(hero, "up")
      |> assert_hero_moved_to_position({1, 2}, game)

      game
      |> move_hero(hero, "right")
      |> assert_hero_moved_to_position({2, 1}, game)
    end

    test "doesn not move through walls", %{game: game, hero: hero} do
      game
      |> move_hero(hero, "left")
      |> assert_hero_moved_to_position({1, 1}, game)
      |> move_hero(hero, "down")
      |> assert_hero_moved_to_position({1, 1}, game)
    end
  end

  defp game(context) do
    {:ok, Map.put(context, :game, build_game())}
  end

  defp hero(context) do
    {:ok, Map.put(context, :hero, build_hero())}
  end

  defp assert_hero_moved_to_position(hero, position, game) do
    assert hero.position == position

    game
  end
end
