defmodule LeagueOfPids.Core.ArenaTest do
  use ExUnit.Case, async: true

  use LeagueOfPids.Factory

  import Arena

  test "builds arena" do
    assert %Arena{} = build_arena()
  end

  test "arena is surrounded by walls" do
    arena = %{size: {width, height}} = build_arena()

    for x <- 0..(width - 1),
        y <- 0..(height - 1),
        x == 0 || y == 0 || x == width - 1 || y == height - 1 do
      assert Enum.member?(arena.walls, {x, y})
    end
  end

  test "can't move through walls" do
    arena = build_arena()

    assert can_move_to_position?(arena, {1, 1})
    refute can_move_to_position?(arena, {0, 0})
  end
end
