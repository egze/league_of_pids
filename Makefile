.PHONY: test console format deps deps_update deps_clean server start stop setup migrate

start:
	docker-compose up -d --build

stop:
	docker-compose down --remove-orphans

setup:
	docker-compose exec app npm install --prefix=assets
	docker-compose exec app mix do deps.get, compile

shell:
	docker-compose exec app bash

console:
	docker-compose exec app iex -S mix

format:
	docker-compose exec app mix format

server:
	docker-compose exec app mix phx.server

test:
	docker-compose exec app mix test

deps:
	docker-compose exec app mix deps.get

deps_update:
	docker-compose exec app mix deps.update --all

deps_clean:
	docker-compose exec app mix deps.clean --unlock --unused

deps_outdated:
	docker-compose exec app mix hex.outdated

check_all:
	docker-compose exec app mix do format, credo, dialyzer
