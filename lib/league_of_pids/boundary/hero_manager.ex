defmodule LeagueOfPids.Boundary.HeroManager do
  use GenServer

  @game_topic "game"
  @shots_topic "game:shot_fired"
  @killed_topic "game:hero_killed"

  alias LeagueOfPids.Boundary.HeroDynamicSupervisor
  alias LeagueOfPids.Core.{Game, Hero}

  def child_spec(hero_name) do
    %{
      id: {__MODULE__, hero_name},
      start: {__MODULE__, :start_link, [hero_name]},
      restart: :transient
    }
  end

  def start_link(hero_name) do
    GenServer.start_link(__MODULE__, hero_name, name: via(hero_name))
  end

  def spawn_hero(hero_name) do
    start =
      DynamicSupervisor.start_child(
        HeroDynamicSupervisor,
        {__MODULE__, hero_name}
      )

    case start do
      {:error, {:already_started, pid}} ->
        {:ok, pid}

      _ ->
        start
    end
  end

  def via(hero_name) do
    {:via, Registry, {LeagueOfPids.Registry, hero_name}}
  end

  def init(hero_name) do
    Phoenix.PubSub.subscribe(LeagueOfPids.PubSub, @shots_topic)
    {:ok, Hero.new(name: hero_name)}
  end

  def get_hero(hero_name) do
    GenServer.call(via(hero_name), :get_hero)
  end

  def move_hero(hero_name, {game, direction}) do
    GenServer.call(via(hero_name), {:move_hero, {game, direction}})
  end

  def shoot(hero_name) do
    GenServer.cast(via(hero_name), :shoot)
  end

  def handle_call(:get_hero, _from, hero) do
    {:reply, hero, hero}
  end

  def handle_call({:move_hero, {game, direction}}, _from, hero) do
    hero = Game.move_hero(game, hero, direction)
    broadcast_change()
    {:reply, hero, hero}
  end

  def handle_cast(:shoot, hero) do
    broadcast_shoot(hero.position)
    {:noreply, hero}
  end

  def handle_info({:shot_fired, position}, hero) do
    hero = Hero.shot_fired(hero, position)

    unless hero.alive do
      broadcast_killed(hero)
      schedule_respawn()
    end

    {:noreply, hero}
  end

  def handle_info(:respawn, hero) do
    broadcast_change()
    {:noreply, %{hero | alive: true}}
  end

  defp schedule_respawn do
    Process.send_after(self(), :respawn, 5_000)
  end

  defp broadcast_killed(hero) do
    Phoenix.PubSub.broadcast(LeagueOfPids.PubSub, @killed_topic, {:hero_killed, hero})
  end

  defp broadcast_change do
    Phoenix.PubSub.broadcast(LeagueOfPids.PubSub, @game_topic, :changed)
  end

  defp broadcast_shoot(position) do
    Phoenix.PubSub.broadcast_from(
      LeagueOfPids.PubSub,
      self(),
      @shots_topic,
      {:shot_fired, position}
    )
  end
end
