defmodule LeagueOfPids.Boundary.GameManager do
  use GenServer

  alias LeagueOfPids.Boundary.HeroManager
  alias LeagueOfPids.Core.Game

  def start_link(_) do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def init([]) do
    DynamicSupervisor.start_link(
      name: LeagueOfPids.Boundary.HeroDynamicSupervisor,
      strategy: :one_for_one
    )

    {:ok, Game.new()}
  end

  def get_game do
    GenServer.call(__MODULE__, :get_game)
  end

  def handle_call(:get_game, _from, game) do
    {:reply, game, game}
  end

  def add_hero(hero_name) do
    HeroManager.spawn_hero(hero_name)
  end

  def get_hero_names do
    Registry.select(LeagueOfPids.Registry, [{{:"$1", :_, :_}, [], [:"$1"]}])
  end

  def get_heroes do
    get_hero_names()
    |> Enum.map(fn hero_name ->
      HeroManager.get_hero(hero_name)
    end)
  end
end
