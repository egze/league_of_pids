defmodule LeagueOfPids.Core.NameGenerator do
  def generate do
    [
      [
        "slow",
        "fast",
        "green",
        "pink",
        "funny",
        "sad"
      ],
      [
        "serverless",
        "elixir",
        "php",
        "cobol",
        "java",
        "js",
        "css",
        "cloud",
        "docker",
        "erlang",
        "core",
        "functional",
        "swift",
        "compiled",
        "beam",
        "message"
      ]
    ]
    |> Enum.map(fn names -> Enum.random(names) end)
    |> Enum.join("-")
  end
end
