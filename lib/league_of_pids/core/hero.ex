defmodule LeagueOfPids.Core.Hero do
  @moduledoc """
    A struct representing a hero.
  """

  use TypedStruct

  alias __MODULE__
  alias LeagueOfPids.Core.NameGenerator

  @typedoc "A hero"
  typedstruct do
    field(:name, String.t(), enforce: true)
    field(:position, {non_neg_integer(), non_neg_integer()}, default: {1, 1})
    field(:alive, boolean(), default: true)
  end

  def new(fields \\ []) do
    %Hero{
      name: Keyword.get(fields, :name, NameGenerator.generate())
    }
  end

  def shot_fired(hero, shot_position) do
    case shot_lethal?(hero, shot_position) do
      true -> %{hero | alive: false}
      false -> hero
    end
  end

  defp shot_lethal?(%{position: {x, y}} = _hero, {shot_x, shot_y} = _shot_position) do
    x_blast_range = Range.new(shot_x - 1, shot_x + 1)
    y_blast_range = Range.new(shot_y - 1, shot_y + 1)

    Enum.member?(x_blast_range, x) && Enum.member?(y_blast_range, y)
  end

  def move_to_position(hero, position) do
    %{hero | position: position}
  end

  def position_left(%{position: {x, y}}) do
    {x - 1, y}
  end

  def position_right(%{position: {x, y}}) do
    {x + 1, y}
  end

  def position_up(%{position: {x, y}}) do
    {x, y + 1}
  end

  def position_down(%{position: {x, y}}) do
    {x, y - 1}
  end
end
