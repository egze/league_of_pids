defmodule LeagueOfPids.Core.Arena do
  @moduledoc """
    A struct representing an arena.
  """

  use TypedStruct

  alias __MODULE__

  @typedoc "An arena"
  typedstruct do
    field(:size, {non_neg_integer(), non_neg_integer()}, default: {10, 10})
    field(:walls, list({non_neg_integer(), non_neg_integer()}), default: [])
  end

  def new(fields \\ []) do
    struct!(Arena, fields)
    |> maybe_generate_walls()
  end

  @doc """
  Checks if the position of possible move is a wall.
  """
  def can_move_to_position?(arena, position) do
    case Enum.member?(arena.walls, position) do
      true -> false
      false -> true
    end
  end

  defp maybe_generate_walls(%{walls: [], size: {width, height}} = arena) do
    %{arena | walls: generate_walls(width, height)}
  end

  defp maybe_generate_walls(arena), do: arena

  # Simple wall around a rectangle.
  defp generate_walls(width, height) do
    for x <- 0..(width - 1),
        y <- 0..(height - 1),
        x == 0 || y == 0 || x == width - 1 || y == height - 1,
        do: {x, y}
  end
end
