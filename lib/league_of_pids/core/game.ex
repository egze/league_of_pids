defmodule LeagueOfPids.Core.Game do
  @moduledoc """
    A struct representing a game.
  """

  use TypedStruct

  alias __MODULE__
  alias LeagueOfPids.Core.{Arena, Hero}

  @typedoc "A game"
  typedstruct do
    field(:arena, Arena.t(), default: Arena.new())
  end

  def new(fields \\ []) do
    struct!(Game, fields)
  end

  def move_hero(_game, %Hero{alive: false} = hero, _direction), do: hero

  def move_hero(%Game{arena: arena}, hero, "up") do
    new_position = Hero.position_up(hero)

    do_move_hero(hero, arena, new_position)
  end

  def move_hero(%Game{arena: arena}, hero, "down") do
    new_position = Hero.position_down(hero)

    do_move_hero(hero, arena, new_position)
  end

  def move_hero(%Game{arena: arena}, hero, "left") do
    new_position = Hero.position_left(hero)

    do_move_hero(hero, arena, new_position)
  end

  def move_hero(%Game{arena: arena}, hero, "right") do
    new_position = Hero.position_right(hero)

    do_move_hero(hero, arena, new_position)
  end

  defp do_move_hero(hero, arena, position) do
    case Arena.can_move_to_position?(arena, position) do
      true -> Hero.move_to_position(hero, position)
      false -> hero
    end
  end
end
