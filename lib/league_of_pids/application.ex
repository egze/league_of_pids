defmodule LeagueOfPids.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Telemetry supervisor
      LeagueOfPidsWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: LeagueOfPids.PubSub},
      # Start the Endpoint (http/https)
      LeagueOfPidsWeb.Endpoint,
      # Start a worker by calling: LeagueOfPids.Worker.start_link(arg)
      # {LeagueOfPids.Worker, arg}
      LeagueOfPids.Boundary.GameManager,
      {Registry, [name: LeagueOfPids.Registry, keys: :unique]}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: LeagueOfPids.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    LeagueOfPidsWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
