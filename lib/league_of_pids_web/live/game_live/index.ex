defmodule LeagueOfPidsWeb.GameLive.Index do
  use LeagueOfPidsWeb, :live_view

  alias LeagueOfPids.Boundary.{GameManager, HeroManager}
  alias LeagueOfPids.Core.NameGenerator

  @game_topic "game"
  @killed_topic "game:hero_killed"

  @impl true
  def mount(_params, _session, socket) do
    game = GameManager.get_game()
    heroes = GameManager.get_heroes()
    {width, height} = game.arena.size

    if connected?(socket) do
      Phoenix.PubSub.subscribe(LeagueOfPids.PubSub, @game_topic)
      Phoenix.PubSub.subscribe(LeagueOfPids.PubSub, @killed_topic)
    end

    {:ok,
     socket
     |> assign(:game, game)
     |> assign(:arena_width, width)
     |> assign(:arena_height, height)
     |> assign(:heroes, heroes)
     |> assign(:hero_name, nil)}
  end

  def handle_params(%{"name" => ""}, _uri, socket) do
    {:noreply,
     socket
     |> push_redirect(to: Routes.game_index_path(socket, :index, name: NameGenerator.generate()))}
  end

  @impl true
  def handle_params(%{"name" => hero_name}, _uri, socket) do
    GameManager.add_hero(hero_name)

    {:noreply, update(socket, :hero_name, fn _ -> hero_name end)}
  end

  @impl true
  def handle_params(_params, _uri, socket) do
    {:noreply, socket}
  end

  @impl true
  def handle_event("move", %{"value" => direction}, socket) do
    hero_name = socket.assigns.hero_name
    game = socket.assigns.game

    HeroManager.move_hero(hero_name, {game, direction})

    {:noreply, socket}
  end

  @impl true
  def handle_event("shoot", %{"value" => hero_name}, socket) do
    HeroManager.shoot(hero_name)

    {:noreply, socket}
  end

  @impl true
  def handle_info(:changed, socket) do
    heroes = GameManager.get_heroes()

    {:noreply, update(socket, :heroes, fn _ -> heroes end)}
  end

  @impl true
  def handle_info({:hero_killed, _hero}, socket) do
    heroes = GameManager.get_heroes()

    {:noreply, update(socket, :heroes, fn _ -> heroes end)}
  end

  defp cell_class(game, heroes, own_hero_name, {x, y}) do
    cond do
      Enum.member?(game.arena.walls, {x, y}) -> "wall"
      cell_with_dead_hero?(heroes, {x, y}) -> "hero_dead"
      cell_with_own_hero?(heroes, own_hero_name, {x, y}) -> "hero"
      true -> ""
    end
  end

  defp display_hero(heroes, own_hero_name, {x, y}) do
    heroes_for_position =
      heroes
      |> Enum.filter(fn hero ->
        hero.position == {x, y}
      end)

    own_hero =
      heroes_for_position
      |> Enum.find(fn hero ->
        hero.name == own_hero_name
      end)

    case {own_hero, List.first(heroes_for_position)} do
      {hero, _} when not is_nil(hero) -> hero.name
      {_, hero} when not is_nil(hero) -> hero.name
      {nil, nil} -> ""
    end
  end

  defp cell_with_own_hero?(heroes, own_hero_name, {x, y}) do
    heroes
    |> Enum.find(fn hero ->
      hero.name == own_hero_name && hero.position == {x, y}
    end)
  end

  defp cell_with_dead_hero?(heroes, {x, y}) do
    heroes
    |> Enum.find(fn hero ->
      hero.alive == false && hero.position == {x, y}
    end)
  end
end
